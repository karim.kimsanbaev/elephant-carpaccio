namespace FizzBuzz.Tests;

public class Tests
{
    [Test]
    public void MainTest()
    {
        var fizzBuzz = new FizzBuzz();

        for (var number = 1; number <= 100; number++)
        {
            var output = fizzBuzz.FindTagDebug(number);
            Console.WriteLine(output);

            if (number % 3 == 0 && number % 5 == 0)
            {
                Assert.AreEqual($"Для числа {number} - Тэг FizzBuzz", output);
            }
            else if (number % 3 == 0)
            {
                Assert.AreEqual($"Для числа {number} - Тэг Fizz", output);
            }
            else if (number % 5 == 0)
            {
                Assert.AreEqual($"Для числа {number} - Тэг Buzz", output);
            }
            else
            {
                Assert.AreEqual($"Для числа {number} - Тэг {number}", output);
            }
        }
    }
}