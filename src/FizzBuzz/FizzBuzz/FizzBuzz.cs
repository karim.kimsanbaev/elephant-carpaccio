﻿namespace FizzBuzz;

public class FizzBuzz
{
    private Rules _rules;

    public FizzBuzz()
    {
        var ruleThree = new ModOn(3);
        //_rules.Add(ruleThree);

        var ruleFive = new ModOn(5);
        //_rules.Add(ruleFive);
    }

    public string FindTagDebug(int number)
    {
        var tag = FindTag(number);
        return $"Для числа {number} - {tag}";
    }

    private ITag FindTag(int number)
    {
        var ruleThree = new ModOn(3);
        var ruleFive = new ModOn(5);
        var ruleOnTheeAndFive = new And(ruleThree, ruleFive);

        if (ruleOnTheeAndFive.IsSuccess(number))
        {
            return new StringTag("FizzBuzz");
        }

        if (ruleThree.IsSuccess(number))
        {
            return new FizzTag();
        }

        if (ruleFive.IsSuccess(number))
        {
            return new BuzzTag();
        }

        return new NumberTag(number);
    }
}

public class StringTag : ITag
{
    private readonly string _tag;

    public StringTag(string tag)
    {
        _tag = tag;
    }

    public override string ToString()
    {
        return $"Тэг {_tag}";
    }
}

public class Rules
{
    private readonly List<IRule> _rules = new();

    public void Add(IRule rule)
    {
        _rules.Add(rule);
    }
}

public interface IRule
{
    bool IsSuccess(int number);
}

public class ModOn : IRule
{
    private readonly int _modNumber;

    public ModOn(int modNumber)
    {
        _modNumber = modNumber;
    }

    public bool IsSuccess(int number)
    {
        if (number % _modNumber == 0)
        {
            return true;
        }

        return false;
    }
}

public class And : IRule
{
    private readonly IRule _left;
    private readonly IRule _right;

    public And(IRule left, IRule right)
    {
        _left = left;
        _right = right;
    }

    public bool IsSuccess(int number)
    {
        if (_left.IsSuccess(number) && _right.IsSuccess(number))
        {
            return true;
        }

        return false;
    }
}

public interface ITag
{
}

public class FizzTag : ITag
{
    public override string ToString()
    {
        return $"Тэг Fizz";
    }
}

public class BuzzTag : ITag
{
    public override string ToString()
    {
        return $"Тэг Buzz";
    }
}

public class NumberTag : ITag
{
    private readonly int _number;

    public NumberTag(int number)
    {
        _number = number;
    }

    public override string ToString()
    {
        return $"Тэг {_number}";
    }
}