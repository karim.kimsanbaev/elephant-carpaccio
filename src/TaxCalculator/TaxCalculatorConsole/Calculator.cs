﻿namespace TaxCalculatorConsole;

public class Calculator
{
    public double EvaluateTotalPrice(double price, int count, string taxState)
    {
        var totalPrice = price * count;
        totalPrice -= GetDiscount(totalPrice);

        if (totalPrice >= 1000)
        {
            totalPrice -= totalPrice / 100 * 3;
        }

        return totalPrice - GetTaxPercent(totalPrice, taxState);
    }

    private double GetDiscount(double totalPrice)
    {
        switch (totalPrice)
        {
            case >= 1000 and < 5000: return totalPrice / 100 * 3;
            case >= 5000 and < 7000: return totalPrice / 100 * 5;
            case >= 7000 and < 10000: return totalPrice / 100 * 7;
            case >= 10000 and < 50000: return totalPrice / 100 * 10;
            case >= 50000: return totalPrice / 100 * 15;
            default: return 0;
        }
    }

    private double GetTaxPercent(double totalPrice, string taxState)
    {
        switch (taxState)
        {
            case "UT": return totalPrice / 100 * 6.85;
            case "NV": return totalPrice / 100 * 8;
            case "TX": return totalPrice / 100 * 6.25;
            case "AL": return totalPrice / 100 * 4;
            case "CA": return totalPrice / 100 * 8.85;
            default: return 0;
        }
    }
}