﻿// See https://aka.ms/new-console-template for more information

TaxCalculatorConsole.Program.InnerMain(args);

namespace TaxCalculatorConsole
{
    public partial class Program
    {
        public static void InnerMain(string[] args)
        {
            var price = double.Parse(args[0]);
            var count = int.Parse(args[1]);
            var taxState = args[2];

            var calculator = new Calculator();

            var totalPrice = calculator.EvaluateTotalPrice(price, count, taxState);

            Console.WriteLine($"Ваша цена {totalPrice}");
        }
    }
}