﻿namespace TaxCalculatorWebApi;

public class Calculator
{
    public double EvaluateTotalPrice(double price, int count, double taxInPercent)
    {
        var totalPriceWithoutDiscount = price * count;

        var totalPriceWithoutTax = totalPriceWithoutDiscount - (totalPriceWithoutDiscount / 100 * 3);
        var taxByPrice = totalPriceWithoutTax / 100 * taxInPercent;

        return totalPriceWithoutTax - taxByPrice;
    }
}