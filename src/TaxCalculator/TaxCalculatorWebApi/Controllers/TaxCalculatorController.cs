﻿using Microsoft.AspNetCore.Mvc;

namespace TaxCalculatorWebApi.Controllers;

[Controller]
[Route("[controller]")]
public class TaxCalculatorController : ControllerBase
{
    [HttpGet]
    public ActionResult<string> Evaluate(double price)
    {
        var totalPrice = price;

        return $"Ваша цена {totalPrice}";
    }
}