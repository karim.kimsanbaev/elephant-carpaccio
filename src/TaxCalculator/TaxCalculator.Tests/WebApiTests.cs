using System.Net.Http.Json;
using Microsoft.AspNetCore.Mvc.Testing;

namespace tests;

[TestFixture]
public class WebApiTests
{
    private HttpClient _client;

    [SetUp]
    public void SetUp()
    {
        var factory = new WebApplicationFactory<Program>();
        _client = factory.CreateClient();
    }

    [Test]
    public async Task Test1()
    {
        var price = 10;

        var response = await _client.PostAsJsonAsync("TaxCalculator", price);
        response.EnsureSuccessStatusCode();

        var result = await response.Content.ReadAsStringAsync();

        Assert.AreEqual(price.ToString(), result);
    }
}